const testCannotRunErr = {
  tacnost: "0%",
  greske: "Testovi se ne mogu izvršiti",
};

const getTestsReport = (passed, failed, greske) => {
  const value = (passed / (passed + failed)) * 100;
  const percentage = Math.round(value * 10) / 10;

  return JSON.stringify({
    tacnost: `${percentage}%`,
    greske,
  });
};

const getJsonFromString = (jsonString) => {
  let json;

  try {
    json = JSON.parse(jsonString);
  } catch {
    return testCannotRunErr;
  }

  if (!json?.tests || json.tests.find((test) => Object.keys(test.err).length))
    return testCannotRunErr;

  return json;
};

const areTestsIdentical = (result1, result2) => {
  // asuming that test couldn't compile we can use getJsonFromString function (the err object is empty in each test)
  const json1 = getJsonFromString(result1);
  const json2 = getJsonFromString(result2);

  if (json1.tests.length !== json2.tests.length) return false;

  return !json1.tests
    .map(
      (test) =>
        json2.tests.find((json2Test) => json2Test.title === test.title) !==
        undefined
    )
    .includes(false);
};

const getNonCommonFailures = (testsObj1, failures) => {
  return failures
    .map((failure) =>
      testsObj1.tests.find((test) => test.title === failure.title) === undefined
        ? failure
        : null
    )
    .filter((failure) => failure != null);
};

class TestoviParser {
  dajTacnost(jsonString) {
    const json = getJsonFromString(jsonString);

    if (json?.greske) return JSON.stringify(json);

    const { passes, failures } = json.stats;
    return getTestsReport(
      passes,
      failures,
      json.failures.map((failure) => failure.title)
    );
  }

  porediRezultate(result1, result2) {
    let percentage = 0;
    let errors = [];

    const obj1 = getJsonFromString(result1);
    const obj2 = getJsonFromString(result2);

    if (obj1?.greske || obj2?.greske) return obj1;

    if (areTestsIdentical(result1, result2)) {
      percentage = obj2.tacnost.replace("%", "");
      errors = obj1.greske;
    } else {
      const obj1FailuresNonCommon = getNonCommonFailures(obj2, obj1.failures);
      const obj2FailuresNonCommon = getNonCommonFailures(obj1, obj2.failures);

      percentage =
        ((obj1FailuresNonCommon.length + obj2.failures.length) /
          (obj1FailuresNonCommon.length + obj2.tests.length)) *
        100;

      percentage = Math.round(percentage * 10) / 10;

      // spread operator is overriding, meaning we are basically creating a Set
      errors = [...obj2FailuresNonCommon, ...obj1FailuresNonCommon].map(
        (failure) => failure.title
      );
    }

    return JSON.stringify({
      promjena: `${percentage}%`,
      greske: errors,
    });
  }
}
