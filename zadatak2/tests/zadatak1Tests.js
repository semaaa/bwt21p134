const assert = chai.assert;

//file examples
const example1 = {
  stats: {
    suites: 2,
    tests: 2,
    passes: 2,
    pending: 0,
    failures: 0,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [],
  passes: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
};

const example2 = {
  stats: {
    suites: 2,
    tests: 2,
    passes: 1,
    pending: 0,
    failures: 1,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  passes: [
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
};

const example3 = {
  stats: {
    suites: 2,
    tests: 2,
    passes: 1,
    pending: 0,
    failures: 1,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: { message: "error happened" },
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  passes: [
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
};

const example4 = {
  stats: {
    suites: 2,
    tests: 3,
    passes: 2,
    pending: 0,
    failures: 1,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  passes: [
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "rand test",
      fullTitle: "Tabela crtaj() rand test",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
};

const example5 = {
  stats: {
    suites: 2,
    tests: 3,
    passes: 0,
    pending: 0,
    failures: 3,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "rand test",
      fullTitle: "Tabela crtaj() rand test",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  passes: [],
};

describe("TestoviParser", () => {
  const runTest = ({ testDescr, example, assertion }) => {
    it(testDescr, () => {
      assert.equal(
        new TestoviParser().dajTacnost(JSON.stringify(example)),
        assertion
      );
    });
  };

  const testCases = [
    {
      testDescr: "Svi testovi prolaze",
      example: example1,
      assertion: '{"tacnost":"100%","greske":[]}',
    },
    {
      testDescr: "Prolazi 50% testova",
      example: example2,
      assertion:
        '{"tacnost":"50%","greske":["should draw 3 rows when parameter are 2,3"]}',
    },
    {
      testDescr: "Error u izvršavanju testova",
      example: example3,
      assertion: '{"tacnost":"0%","greske":"Testovi se ne mogu izvršiti"}',
    },
    {
      testDescr: "Testiranje da li se zaokružuje tacnost na jednu decimalu",
      example: example4,
      assertion:
        '{"tacnost":"66.7%","greske":["should draw 3 rows when parameter are 2,3"]}',
    },
    {
      testDescr: "Trebaju svi testovi da su pali",
      example: example5,
      assertion:
        '{"tacnost":"0%","greske":["should draw 3 rows when parameter are 2,3","should draw 2 columns in row 2 when parameter are 2,3","rand test"]}',
    },
  ];

  testCases.forEach(runTest);
});
