const testCannotRunErr = {
  tacnost: "0%",
  greske: "Testovi se ne mogu izvršiti",
};

const getTestsReport = (passed, failed, greske) => {
  const value = (passed / (passed + failed)) * 100;
  const percentage = Math.round(value * 10) / 10;

  return JSON.stringify({
    tacnost: `${percentage}%`,
    greske,
  });
};

const getJsonFromString = (jsonString) => {
  let json;

  try {
    json = JSON.parse(jsonString);
  } catch {
    return testCannotRunErr;
  }

  if (!json?.tests || json.tests.find((test) => Object.keys(test.err).length))
    return testCannotRunErr;

  return json;
};

class TestoviParser {
  dajTacnost(jsonString) {
    const json = getJsonFromString(jsonString);

    if (json?.greske) return JSON.stringify(json);

    const { passes, failures } = json.stats;
    return getTestsReport(
      passes,
      failures,
      json.failures.map((failure) => failure.title)
    );
  }
}
