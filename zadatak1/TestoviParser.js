const testCannotRunErr = {
  tacnost: "0%",
  greske: "Testovi se ne mogu izvršiti",
};

//primjer testiranja, terminal
const primjer = {
  stats: {
    suites: 2,
    tests: 2,
    passes: 2,
    pending: 0,
    failures: 0,
    start: "2021-11-05T15:00:26.343Z",
    end: "2021-11-05T15:00:26.352Z",
    duration: 9,
  },
  tests: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
  pending: [],
  failures: [],
  passes: [
    {
      title: "should draw 3 rows when parameter are 2,3",
      fullTitle: "Tabela crtaj() should draw 3 rows when parameter are 2,3",
      file: null,
      duration: 1,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
    {
      title: "should draw 2 columns in row 2 when parameter are 2,3",
      fullTitle:
        "Tabela crtaj() should draw 2 columns in row 2 when parameter are 2,3",
      file: null,
      duration: 0,
      currentRetry: 0,
      speed: "fast",
      err: {},
    },
  ],
};

const getTestsReport = (passed, failed, greske) => {
  const value = (passed / (passed + failed)) * 100;
  const percentage = Math.round(value * 10) / 10;

  return JSON.stringify({
    tacnost: `${percentage}%`,
    greske,
  });
};

const getJsonFromString = (jsonString) => {
  let json;

  try {
    json = JSON.parse(jsonString);
  } catch {
    return testCannotRunErr;
  }

  if (!json?.tests || json.tests.find((test) => Object.keys(test.err).length))
    return testCannotRunErr;

  return json;
};

class TestoviParser {
  dajTacnost(jsonString) {
    const json = getJsonFromString(jsonString);

    if (json?.greske) return JSON.stringify(json);

    const { passes, failures } = json.stats;
    return getTestsReport(
      passes,
      failures,
      json.failures.map((failure) => failure.title)
    );
  }
}

console.log(new TestoviParser().dajTacnost(JSON.stringify(primjer)));